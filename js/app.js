$(document).ready(function() {
    $(function() {
        new WOW().init();
    });
    $(".owl-carousel").owlCarousel({
        items: 1,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 2000,
        autoWidth: false,
        center: true,
        dots: false,
        // responsiveRefreshRate: 100,
        responsive: {
            768: {
                option1: 80
            }
        },
        // mergeFit: false,
        loop: true

    });
    let owl = $('.owl-carousel');
    $('.backward').click(function() {
        owl.trigger('next.owl.carousel');
    })
    $('.forward').click(function() {
        owl.trigger('prev.owl.carousel');
    })

    function burgerMenu(selector) {
        let menu = $(selector);
        let button = menu.find('.burger-menu_button', '.burger-menu_lines');
        let links = menu.find('.link');
        let overlay = menu.find('.burger-menu_overlay');

        button.on('click', (e) => {
            e.preventDefault();
            toggleMenu();
        });

        links.on('click', () => toggleMenu());
        overlay.on('click', () => toggleMenu());

        function toggleMenu() {
            menu.toggleClass('burger-menu_active');
            button.toggleClass('burger-menu_button_active')
            if (menu.hasClass('burger-menu_active')) {
                $('body').css('overlow', 'hidden');
            } else {
                $('body').css('overlow', 'visible');
            }
        }
    }

    burgerMenu('.burger-menu');
    // $(".switch-text").click(function() {
    //     $('.btn_bg').toggleClass('btn-bg_active');
    //     $(".switch-text").css("color", "#616368");
    //     $(this).css("color", "white")
    //     $(this).attr('data-item-name')
    // });

    $("#btnR").on("click", function() {
        if ($(this).hasClass('active-btn')) {
            console.log(123)
            return false;
        }
        $('.btn_bg').addClass('btn-bg_active');
        $('.switch-text').removeClass('active-btn');
        $(this).addClass('active-btn');
        $("#btnR").css("color", "white");
        $("#btnL").css("color", "#616368");
        $(".exFeatures-offers").fadeOut();
        $(".exFeatures-offers").fadeIn();
        setTimeout(() => $(".top-month").html("/ year"), 400);
    })
    $("#btnL").on("click", function() {
        if ($(this).hasClass('active-btn')) {
            console.log(123)
            return false;
        }
        $('.btn_bg').removeClass('btn-bg_active');
        $('.switch-text').removeClass('active-btn');
        $(this).addClass('active-btn');
        $("#btnL").css("color", "white");
        $("#btnR").css("color", "#616368");
        $(".exFeatures-offers").fadeOut();
        $(".exFeatures-offers").fadeIn();
        setTimeout(() => $(".top-month").html("/ month"), 400);
    })
    $("#arr1").on('click', function(){
        $(".exFeatures-offer").toggleClass('offer-active');
    })
    $("#arr2").on('click', function(){
        $(".exFeatures-offer").toggleClass('offer-active');
    })
});